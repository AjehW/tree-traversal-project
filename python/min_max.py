from binary_node import BinaryNode as Node

n1 = Node(value=2)
n2 = Node(value=5)
n3 = Node(value=1, left=n1, right=n2)
n4 = Node(value=7)
n5 = Node(value=3)
n6 = Node(value=6, left=n4, right=n5)
n7 = Node(value=4, left=n3, right=n6)

graph = n7

'''
Traverse the tree to find the minimum value in the tree
'''
def min_of_tree(root_node):
    minimum = float('inf')
    stack = [root_node]
    while stack:
        # pop an item off the queue
        node = stack.pop()
        # compare the node's value to the target_value
        if node.value < minimum:
            minimum = node.value
        # If they're not the same, add the childrem to the queue
        if node.left:
            stack.append(node.left)
        if node.right:
            stack.append(node.right)
    return minimum
'''
Traverse the tree to find the maximum value in the tree
'''
def max_of_tree(root_node):
    maximum = float('-inf')
    stack = [root_node]
    while stack:
        # pop an item off the queue
        node = stack.pop()
        # compare the node's value to the target_value
        if node.value > maximum:
            maximum = node.value
        # If they're not the same, add the childrem to the queue
        if node.left:
            stack.append(node.left)
        if node.right:
            stack.append(node.right)
    return maximum

# Pass these tests
assert min_of_tree(graph) == 1
assert max_of_tree(graph) == 7
